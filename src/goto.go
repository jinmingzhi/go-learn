package main

import (
	"fmt"
)

func main() {
	// 使用标签和 goto 语句是不被鼓励的s

	// 一下是无限死循环
	//	one: // lable 标签
	//		fmt.Println("One")
	// 	goto one // 跳到one 执行

	/*
		goto two   // 直接跳到two 执行
		fmt.Println("还有谁")
		two:
			fmt.Println("two")
	*/

}
