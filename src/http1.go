package main

import (
	"io"
	"log"
	"net/http"
)

const form = `
	<html><body>
		<form action="#" method="post" name="bar">
			<input type="text" name="in" />
			<input type="submit" value="submit"/>
		</form>
	</body></html>
`

func test(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	switch r.Method {
	case "GET":
		io.WriteString(w, form)
	case "POST":
		// io.WriteString(w, "hello world")
		io.WriteString(w, r.FormValue("in"))
	}
}

func main() {
	http.HandleFunc("/test", test)
	err := http.ListenAndServe("127.0.0.1:8080", nil)
	if err != nil {
		log.Fatal("listenAndServe: ", err.Error())
	}

}
