package main

import (
	"fmt"
)

// new 分配一个数据类型内存,内存的数据类型为零值,并返回指向该内存的指针.
// make 返回是一个引用,可初始化内存数据
//
func main() {
	var p1 *int = new(int) // p1 是指针 *p1 才是内存空间
	var p2 int             // p2 内存空间

	fmt.Println(p1) // 0xc000012060
	fmt.Println(p2) // 0
}
