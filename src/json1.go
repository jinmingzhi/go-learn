package main

import (
	"encoding/json"
	"fmt"
)

func echo(i interface{}) {
	fmt.Printf("(%v, %T)\n", i, i)
}

func main() {
	// 对json 进行解码操作

	type teacher struct {
		Name   string `json:"teacher_name"`
		Age    int
		school string
	}

	jmz := teacher{"jmz", 23, "上海一中"}
	res, err := json.Marshal(jmz)
	if err != nil {
		panic(err.Error())
	}

	fmt.Println(string(res))

	// 解码
	var s interface{}
	json.Unmarshal(res, &s)
	fmt.Println(s) //map[teacher_name:jmz Age:23]

	s1 := s.(map[string]interface{})         // 空接口的类型转换方式
	fmt.Println(s1["teacher_name"])          // jmz
	echo(s1["teacher_name"])                 // (jmz, string), 此时此刻内部其实还是 interface类型
	fmt.Println(s1["teacher_name"].(string)) // jmz

	m := make(map[string]int)
	m["age"] = 23
	fmt.Println(m)        //map[age:23]
	fmt.Println(m["age"]) // 23
}
