package main

import (
	"fmt"
)

var (
	first, last string
)

// 简单的输入, Scanln
func main() {
	fmt.Println("简单的输入：")
	fmt.Scanln(&first, &last) // 可以输入两个值，已空格隔开，如 aaa bbb 那么first就是指向aaa,last就会指向bbb
	fmt.Println(first)
	fmt.Println(last)
}
