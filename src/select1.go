package main

import (
	"fmt"
	"time"
)

// select
func main() {
	ch := make(chan int)
	timeout := make(chan int, 1)

	go func() {
		time.Sleep(1 * time.Second)
		timeout <- 1
	}()

	// 这里面只是输出一个，需要在用一次才可以输了第二个
	select {
	case <-ch:
		fmt.Println("come to read cn!")
	case <-time.After(2 * time.Second): // 的奶一秒后输出
		fmt.Println("come to timeAfter")
	case <-timeout:
		fmt.Println("comme to timeout")
		// default:
		// 	fmt.Println("comt ti default")
	}

	select {
	case <-ch:
		fmt.Println("come to read cn!")
	case <-time.After(2 * time.Second): // 的奶一秒后输出
		fmt.Println("come to timeAfter")
	case <-timeout:
		fmt.Println("comme to timeout")
		// default:
		// 	fmt.Println("comt ti default")
	}

}
