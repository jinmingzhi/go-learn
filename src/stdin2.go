package main

import (
	"bufio"
	"fmt"
	"os"
)

var inputReader *bufio.Reader

func main() {
	inputReader = bufio.NewReader(os.Stdin) // 创建一个读取器，并将其与标准输入绑定。
	fmt.Printf("please you name:")
	input, err := inputReader.ReadString('\n') // 不论是windows还是linux 都使用\n
	// ReadString(delim byte)，该方法从输入中读取内容，直到碰到 delim 指定的字符
	// 然后将读取到的内容连同 delim 字符一起放到缓冲区。
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("your name is ", input)
}
