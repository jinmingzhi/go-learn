package main

import (
	"fmt"
)

func main() {
	goto LABEL
LABEL:
	for i := 1; i < 10; i++ {
		for j := 1; j <= i; j++ {
			if j == 4 {
				break
			}
			fmt.Printf(" %d * %d = %d ", i, j, i*j)
		}
		fmt.Println()
	}
	return

}
