package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {
	client, err := net.Dial("tcp", "127.0.0.1:8888")
	if err != nil {
		panic("Error dialing" + err.Error())
		return
	}
	defer client.Close()
	for {
		input := bufio.NewScanner(os.Stdin)
		input.Scan()
		client.Write([]byte(input.Text()))
		b := make([]byte, 512)
		client.Read(b)
		fmt.Printf("server:%s\r\n", string(b))
	}
}
