package main
import(
	"fmt"
)

func main() {
	var i int = 0
	LOOP: for i < 10 {   // for 循环  LOOP 标识而已
		i++
		if i == 5 {
			goto LOOP
		} else if i != 3 {
			fmt.Println("LOOP:",i)
		} else {
			fmt.Println("LOOP default:",i)
		}
	}

	var age int = 23
	if age == 25 {
		fmt.Println("true")
	} else if age < 25 {
		fmt.Println("too small")
	} else {
		fmt.Println("too big")
	}
}