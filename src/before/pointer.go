package main

import (
	"fmt"
)

var a string = "a"

// 全局空间不可以这样定义变量
// b:="b"
/*
//b:="b"
//相当于
var b
b = "b"
*/
func main() {
	fmt.Println("a 的引用地址：", &a)
	// 定义指正变量
	var c *int // 对，在类型前加一个* 就是 指针类型
	var temp int = 100
	c = &temp // 将指针变量 c 指向于 temp 一样的引用地址
	temp = 200
	fmt.Println("c:", *c)

	var d *int

	if d == nil { // 判断是否是空指针
		fmt.Println("空指针d", d) // nil    空指针为nil  与 null 一样
	}
}
