package main

import (
	"fmt"
)

func main() {
	// var varibale_name [size] variable_type
	// var 变量名 [数组个数] 数组内每一个值的类型
	// 定的恨死，如果有variable_type 内部就必须都是 该类型

	// 定义1  一次定义后使用
	var array1 = [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	fmt.Println("array1:", array1)
	fmt.Println("array1[0]:", array1[0], "  array[10]:", array1[9])

	// 定义2 先定义，后赋值
	var array2 [3]int
	array2[0] = 1
	array2[1] = 2
	array2[2] = 3
	fmt.Println("array2:", array2)

	// 定义3
	var array3 [3]string
	array3[0] = "a"
	array3[1] = "a"
	array3[2] = "a"
	// var array3 [] string{'a','b','c',d}   这样也可以
	fmt.Println("array3:", array3)

	var array4 = []string{"jmz","上海"}
	fmt.Println("array4 不固定个数:",array4)

}
