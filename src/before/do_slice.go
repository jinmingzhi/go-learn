package main

import (
	"fmt"
)

func main() {
	// 切片类型，增删改查
	array := make([]int, 3, 10)
	array[0] = 5 // 只能改
	array[2] = 6
	array = append(array, 11) // 增
	array = append(array, 12)
	array = append(array, 13)

	array1 := make([]int, 0, cap(array)*2)
	copy(array1, array)

	array2 := append(array, 111) // 添加并返回一个新的值
	fmt.Println(array)
	fmt.Println(array1)
	fmt.Println(array2)
	fmt.Printf("array 引用地址：%T\narray1 引用地址：%T\narray2 引用地址：%T\n", array, array1, array2)
}
