package main

import (
	"fmt"
)

// 定义结构体，相当于定义了一个数据类型
type Circle struct {
	radius float64
}

// 必须放在 main 函数外定义
func (c Circle) getLength() float64 {
	return c.radius * 2 * 3.14
}

func main() {
	
	/*
		// 结构体 变量声明
		var variable_name = structture{value1,value2...}
		or
		variable_name :=structture{key:value1,key:value2,key:value3....}
		// 结构体 赋值or 取值   : 以.作为操作符
		赋值 ：variable_name.key = ???
		取值 ：varibale_name.key
		
		// 结构体指针
	*/

	var c Circle
	c.radius = 10.00

	fmt.Println("length of Circle(c) = ", c.getLength())

	var c1 = Circle{2.77}
	// var c1 = Circle{radius:3.77}
	fmt.Println("c1 radius:",c1.radius)

	pa := &c1  // pa 是 指针类型 
	fmt.Println("pa 的引用地址：",pa)
	fmt.Println("(*pa).radius:",(*pa).radius)
	fmt.Println("pa.radius:",pa.radius)     // pa 明明是指针类型 确可以这样调用，很奇怪吧！！
	/*
		隐式解引用
			对于一些复杂类型的指针， 如果要访问成员变量的话，
			需要写成类似(*p).field的形式，Go提供了隐式解引用特性，
			我们只需要p.field即可访问相应的成员。
	 */

}
