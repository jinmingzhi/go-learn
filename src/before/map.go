package main

import (
	"fmt"
)

func main() {
	// map 集合，无序的
	/*
		定义方式一：
		var map_vaiable_name = map[key_data_type]value_data_value
		map_vaiable_name := make(map[key_data_type]value_data_value)

		定义方式二：
		var map_variable = map[key_data_type]value_data_value{key1:value1,key2:value2....}
	*/
	var maps map[string]int // 表示key是string类型， value是int 类型
	maps = make(map[string]int)
	maps["a"] = 22
	maps["c"] = 33 // 增改都可以
	// 以上可以直接定义成
	/*
		var maps = map[string]int {"a":22,"b":33}
	*/

	fmt.Println(maps)

	k, v := maps["a"] // 判断数据是否存在
	if v {
		fmt.Println(k) // 22
		fmt.Println(v) // true
		fmt.Printf("数据存在")
	} else {
		fmt.Println("数据不存在")
	}

}
