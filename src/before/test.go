package main

import (
	"fmt"
)

// 申明全局变量
var global int = 1214

// 引用传值
func swap(x *int, y *int) {
	var temp int
	temp = *x
	*x = *y
	*y = temp
}

// 定义结构体，相当于定义了一个数据类型
type Circle struct {
	radius float64
}

// 必须放在 main 函数外定义
func (c Circle) getLength() float64 {
	return c.radius * 2 * 3.14
}

func main() {
	var a int = 100
	var b int = 200

	swap(&a, &b)
	fmt.Println("a = %d", a)
	fmt.Println("b = %d", b)

	var c Circle
	c.radius = 10.00

	fmt.Println("length of Circle(c) = ", c.getLength())

	/*	w := "aaa"
		v := &w
		fmt.Println("v == w =", *v)
	*/

	// 申明局部变量
	// 下面的定义变量类型写在最后表示前面都是该数据类型
	var q, p, o int = 1, 2, 3
	/*
		等同于:
			var q int = 1
			var p int = 2
			var 0 int = 3
	*/

	fmt.Println("a,b,c = ", q, p, o)
	fmt.Println("global before = ", global)
	global = p + o
	fmt.Println("global after = ", global)
}
