package main

import (
	"fmt"
)

func main() {
	var x, y int = 7, 4
	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(int8(x / y))
}
