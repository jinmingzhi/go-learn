package main

import (
	"fmt"
)

func main() {
	//Go 语言中 range 关键字用于 for 循环中迭代数组(array)、切片(slice)、通道(channel)或集合(map)的元素。
	//在数组和切片中它返回元素的索引和索引对应的值，
	//    在集合中返回 key-value 对的 key 值。
	var nums = []int{11, 22, 33}
	for i, num := range nums {
		fmt.Println(i, "-->", num)
	}

	var _ = "aa" //                 _ 忽略这个变量.
	for _, num := range nums {
		fmt.Println(num)
	}

	var strs = map[string]string{"a": "jmz", "b": "jly"}
	for k, v := range strs {
		fmt.Printf("%s--->%s\n", k, v)
	}
}
