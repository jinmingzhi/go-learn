package main

import (
	"fmt"
)

func main() {
	// var varibale_name [size] variable_type
	// var 变量名 [数组个数] 数组内每一个值的类型
	// 定的恨死，如果有variable_type 内部就必须都是 该类型

	// 定义1  一次定义后使用
	var array1 = []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,22}

	fmt.Println("array1[:4]:", array1[:4])
	fmt.Println("array1[3:6]:",array1[3:6])
	// len 获取切片长度
	// map 测量切片最长可以达到
	fmt.Printf("array1 有%d个(len)\n",len(array1))
	fmt.Printf("array1 有%d个(map)\n",cap(array1))



	// 定义2 通过内置函数make()初始化切片
	// make([]type,len,map)
	array2 := make([]int,3,10)  // 创建最大个数为10 ，前3下标值为0的切片
	fmt.Println("array2 before:",array2)
	for i:=0;i<3;i++ {  // 超过3 会报错，无法获取到值
		fmt.Printf("array2[%d]:%d\n",i,array2[i])
	}
	// fmt.Printf(array2[3])  // 此时会报错因为在创建时 只创建了3个值为0 的切片 
	array2[0] = 10
	array2[1] = 11
	array2[2] = 12
	fmt.Println("array2 after:",array2)

	fmt.Printf("len=%d cap=%d slice=%v\n",len(array2),cap(array2),array2)


	// 空 切片
	var array3 []string
	if array3 == nil {
		fmt.Println("array3 is nil")
	}

}
