package main

import (
	"fmt"
)

func desc_sum(n uint) uint {
	if n > 0 {
		result := n + desc_sum(n-1)
		return result
	}
	return 0
}

// 二分法
func fen_2(nums []int, num int) int {
	med_int := int(len(nums) / 2) // 表示数组中间个数值，  为0 时 表示没有数据了
	if med_int == 0 {
		return -1
	}

	median := nums[med_int]

	if num < median {
		nums = nums[:med_int]
		return fen_2(nums, num)
	} else if num > median {
		nums = nums[med_int:]
		return fen_2(nums, num)
	} else {
		return num
	}
}

func main() {
	var num uint = 2
	res := desc_sum(num)
	fmt.Println(res)
	fmt.Printf("%T", res)
	fmt.Println("-----------------我是和平的分割线-----------------")

	var nums1 = []int{1, 4, 6, 16, 18, 20, 32, 52, 61, 81, 91, 92, 93, 99, 100}

	fmt.Println(fen_2(nums1, 4))
}
