package main

import (
	"fmt"
	"reflect"
)

// 结论：接口接收的是什么类型，以后只能转当初接收的类型，哈哈哈哈哈

type v interface{} // 自定义数据类型 v
type str string    // 自定义数据类型 str

func main() {
	var v1 v = str("jmz") // jmz 是 str 类型
	// v2 := string("aaa")  // 这是是string类型
	// fmt.Println(v2, reflect.TypeOf(v2))  // aaa string
	if value, ok := v1.(string); ok {
		fmt.Println(value, reflect.TypeOf(value))
	} else {
		fmt.Println("没成功")
	}

	switch v1.(type) {
	case string:
		fmt.Println("string")
	case str:
		fmt.Println("*str")
	default:
		fmt.Println("default")
	}

	/*
	   没成功
	   *str
	*/

}
