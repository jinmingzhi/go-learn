package main

import (
	"fmt"
	// "reflect"
	"strconv"
)

// 自定义错误类型
type parseError struct {
	Index int
	Word  string
	Err   error
}

func (p *parseError) String() string {
	return fmt.Sprintf("pkg parse: error parsing %q as int", p.Word)
}

// 再强调一下
// 1、recover 只能使用在defer中
// 2、recover只能捕获panic 抛出的异常
func main() {
	slice := []string{"123", "111", "kkk"}
	nums := parseSlice(slice)
	fmt.Println(nums)
}

func parseSlice(s []string) (numbers []int) {
	defer func() {
		if r := recover(); r != nil { // 捕获异常后，不处理，返回成功的部分
			return
		}
	}()

	if len(s) == 0 {
		panic("no words to parse")
	}

	for i, v := range s {
		num, err := strconv.Atoi(v)
		if err != nil {
			panic(&parseError{i, v, err})
		}
		numbers = append(numbers, num)
	}
	return
}
