package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func convertTobin(n int) string {
	result := ""
	if n == 0 {
		return "0"
	}
	for ; n > 0; n /= 2 {
		lsb := n % 2
		result = strconv.Itoa(lsb) + result
	}
	return result
}

func printFile(filename string) {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

//func forever() {
//	for {
//		fmt.Println("abc")
//	}
//}

func main() {
	fmt.Println(convertTobin(5))
	fmt.Println(convertTobin(11))
	printFile("abc.txt")
	//forever()
}
