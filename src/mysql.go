package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	// "strconv" // 数字字符串转化
	"time"
)

func main() {
	db, err := sql.Open("mysql", "vagrant:vagrant@tcp(192.168.33.110:3306)/db")
	if err != nil {
		fmt.Println("err", err.Error())
	}
	defer db.Close()

	for i := 0; i < 10; i++ {
		go func() {
			res, err := db.Exec("update student set status = status + 1 where id = 121")
			if err != nil {
				fmt.Println("err:", err.Error())
			}
			aff_nums, _ := res.RowsAffected()
			fmt.Println(aff_nums)
		}()
		// strconv.Itoa(i)
	}

	time.Sleep(4 * time.Second)
}
