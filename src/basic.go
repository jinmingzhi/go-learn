package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	filename := "abc.txt"
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	} else {
		fmt.Printf("%s\n", content)
	}
}
