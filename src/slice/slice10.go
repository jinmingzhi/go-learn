package main

import (
	"fmt"
)

func main() {
	str := "jmz"
	b := make([]byte, len(str))
	copy(b, str)

	fmt.Println(b)         // [106 109 122]
	fmt.Println(string(b)) // jmz

	// 试试下面的
	// str := "jmz"
	// fmt.Println(str[1]) // 109
}
