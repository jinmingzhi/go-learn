package main

import (
	"fmt"
)

// copy 参数必须是切片
// copy 一方的len 必须>= 被copy一方才会吧数据都copy下来
func main() {
	var arr = []int{1, 2, 3, 4, 5}
	// s := make([]int, 0, 10) // []
	// var s []int // == s:= make([]int,0)

	// s := make([]int, 5, 10) // [1 2 3 4 5]
	s := make([]int, len(arr)) // 推荐写法
	copy(s, arr)
	fmt.Println(s)

}
