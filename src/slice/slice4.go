package main

import (
	"fmt"
)

func main() {
	sum, avg := SumAndAverage(2, 3)
	fmt.Println(sum, avg)
}

func SumAndAverage(a, b int) (sum int, avg float32) {
	sum = a + b
	avg = float32(sum) / 2
	return

}
