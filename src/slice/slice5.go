package main

import (
	"fmt"
)

func main() {
	var f1 = []float32{2.4, 2.0, 1.3, 3.33, 45.1}
	fmt.Println(sum(f1...)) // []int...  将数组或切片打撒
}

func sum(f ...float32) (sum float32) { // ... 可变长参数
	for _, v := range f {
		sum += v
	}
	return
}
