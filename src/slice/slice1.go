package main

import (
	"fmt"
)

func main() {
	var arr [6]int

	var slice []int = arr[2:5] // 切片对数组的引用？？
	// 切片是引用类型

	fmt.Println(slice) // [0 0 0]

	for i := 0; i < len(arr); i++ {
		arr[i] = i
	}
	fmt.Println(slice) //[2 3 4]

}
