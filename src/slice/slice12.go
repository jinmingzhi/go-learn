package main

import (
	"fmt"
)

func main() {
	str := "jmz is cool"
	fmt.Println(reverse1(str))
	fmt.Println(reverse2(str))

	str1 := "我是中国人，china"
	fmt.Println(china_reverse(str1))
}

// 反转字符串1
func reverse1(s string) string {
	var str string
	for _, v := range []byte(s) {
		str = string(v) + str
	}
	return str
}

// 反转字符串2
func reverse2(s string) string {
	new_s := []byte(s)
	new_b := make([]byte, len(s))

	for m, n := len(new_s), 0; m > 0; m-- {
		new_b[n] = new_s[m-1]
		n++
	}
	return string(new_b)
}

// 支持中文的字符反转
func china_reverse(s string) string {
	var str string
	for _, v := range []rune(s) {
		str = string(v) + str
	}
	return str
}
