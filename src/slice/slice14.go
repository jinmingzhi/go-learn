package main

import (
	"fmt"
)

func main() {
	s1 := []int{1, 2, 3}
	a := [...]int{1, 2, 3, 4, 5}
	changeSlice(s1)
	changeArr(a)
	fmt.Println(s1) //[1 1111 3]
	fmt.Println(a)  //[1 2 3 4 5]
}

// 切片传的是引用
func changeSlice(s []int) {
	s[1] = 1111
}

// 数组传的是值
func changeArr(a [5]int) {
	a[2] = 22222
}
