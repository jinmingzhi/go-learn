package main

import (
	"fmt"
)

func main() {
	// 切片在内存中的组织方式实际上是一个有三个域的结构体,
	// 1、指向相关数组的指针(其实就是数组的一个连续引用).
	// 2、切片长度
	// 3、切片的容量
	arr := [6]int{1, 2, 3, 4, 5, 6}
	var arr1 []int = arr[2:4]
	fmt.Println(arr1)
}
