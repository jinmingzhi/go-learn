package main

import (
	"fmt"
)

func main() {
	var s = []string{"a", "v", "d"}
	s1 := Appendbyte(s, "j", "m", "z")
	fmt.Println(s1)
}

// 仿写append
func Appendbyte(s []string, data ...string) []string {
	m := len(s)
	n := len(data) + m
	if n > cap(s) {
		new_slice := make([]string, (n+1)*2)
		copy(new_slice, s)
		s = new_slice
	}
	slice := s[0:n]
	copy(slice[m:n], data)
	return slice
}
