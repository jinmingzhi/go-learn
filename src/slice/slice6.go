package main

import (
	"fmt"
)

func main() {
	var ar = [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	var a = ar[5:7] // reference to subarray {5,6} - len(a) is 2 and cap(a) is 5
	fmt.Println(a)  // [5 6]

	a = a[0:4]     // ref of subarray {5,6,7,8} - len(a) is now 4 but cap(a) is still 5
	fmt.Println(a) // [5 6 7 8]

	// b := a[3:3]
	// fmt.Println(b)
}
