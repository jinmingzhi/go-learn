package main

import (
	"fmt"
)

func main() {
	var s1 = []int{1, 2, 3}
	var s2 = []int{11, 22, 33, 44, 55}
	fmt.Println(InsertStringSlice(s1, s2, 6))
}

/**
 * 将切片插入到另一个切片的指定位置
 * @param {[type]} src []int 		   [原数据切片]
 * @param {[type]} dct []int         [目标数据切片]
 * @param {[type]} start int           [开始插入地方]
 */
func InsertStringSlice(src, dct []int, start int) []int {
	if len(dct) < start {
		s := make([]int, start)
		copy(s, dct)
		dct = s
	}

	new_s := dct[start:]
	new_s = append(dct[:start], src...)

	return append(new_s, dct[start:]...)
}
