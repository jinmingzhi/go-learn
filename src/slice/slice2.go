package main

import (
	"fmt"
	"reflect"
)

func main() {
	var arr = [5]int{3: 41, 4: 33}
	s := arr[1:3]. // []int
			f(&s)
	fmt.Println(reflect.TypeOf(s))
}

func f(slice *[]int) {
	for _, v := range *slice {
		fmt.Println(v)
	}
}
