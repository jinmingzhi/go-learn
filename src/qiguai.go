package main

import (
	"fmt"
)

func main() {
	/*
		// // 引用类型变量，这是只是声明，并没有分配空间。
		 var i *int
		 *i=10
		 fmt.Println(*i)
	*/

	var i *int // 引用类型变量，这是只是声明，并没有分配空间。
	var j int
	j = 100
	i = &j          // 将内存地址指向j
	fmt.Println(*i) //panic: runtime error: invalid memory address or nil pointer dereference

	//  在来看看下面
	*i = 1 // 改变的是哪个地址的内存数据
	fmt.Println(j)

}
