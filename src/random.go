package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	timens := time.Now().Nanosecond()
	rand.Seed(123456789) // 为随机数 提供种子，这样才会每次都不一样

	for i := 0; i < 5; i++ {
		fmt.Println(rand.Float32())
		fmt.Println(rand.Intn(2))
	}

}
