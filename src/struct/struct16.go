package main

import (
	"fmt"
)

type queue []int // 自定义类型,可以将自定义类型,看成就是结构体

func (q *queue) push(v int) {
	(*q) = append(*q, v)
}

func (q *queue) pop() int {
	head := (*q)[0]
	*q = (*q)[1:]
	return head
}

func (q *queue) isEmpty() bool {
	return len(*q) == 0
}

func main() {
	q := &queue{1}
	q.push(2)
	q.push(3)
	fmt.Println(q.pop())     // 1
	fmt.Println(q.pop())     // 2
	fmt.Println(q.isEmpty()) // false
	fmt.Println(q.pop())     // 3
	fmt.Println(q.isEmpty()) // true
}
