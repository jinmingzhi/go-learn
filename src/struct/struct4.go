package main

import "fmt"

type Person struct {
	name string
	age  int
}

func (person *Person) getNameAndAge() (string, int) {
	return person.name, person.age
}

type Student struct {
	Person            // 继承了Person 类   // 匿名字段，那么默认Person就包含了person 的所有字段
	speciality string // 专业
}

func (student *Student) getSpeciality() string {
	return student.speciality
}

func main() {
	student := new(Student)
	student.name = "jmz"
	student.age = 23
	student.speciality = "营销策划"
	name, age := student.getNameAndAge()
	fmt.Println(name, age)               // jmz 23
	fmt.Println(student.getSpeciality()) //jmz 23

	person := new(Person)
	person.name = "jzm"
	person.age = 23
	fmt.Println(person.getNameAndAge()) // jzm 23
}
