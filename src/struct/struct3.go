package main

import "fmt"

type Point struct {
	px float32
	py float32
}

//  一定要使用指针的方式（*）接受，如果不使用，就等于重新复制了一份
func (point *Point) setxy(x, y float32) {
	point.px = x
	point.py = y
}

func (point Point) getxy() (float32, float32) {
	return point.px, point.py
}

func main() {
	point := new(Point)
	point.setxy(1.12, 3.34)
	fmt.Println(point.getxy())
}
