package main

import (
	"fmt"
)

const PI float64 = 3.14

type circle struct {
	len float64
}

// 引用转递
func (self *circle) perimeter() float64 {
	return self.len * PI
}

// 值传递
func (self circle) acreage() float64 {
	return 2 * PI * self.len * self.len
}

func main() {
	c1 := circle{9.2}  // 值类型
	c2 := &circle{8.2} // 指针类型
	c3 := new(circle)  // 也是指针
	c3.len = 7.1       // (*c3).len = 7.1  一样，因为结构体的指针类型会解引用

	fmt.Println(c1)
	fmt.Println(c2)
	fmt.Println(c3)

	fmt.Println(c1.perimeter()) // 以前值类型 不可以调用引用传递的函数，  现在居然行了，哈哈哈，尴尬
	fmt.Println(c2.perimeter())
	fmt.Println(c3.perimeter())

	fmt.Println(c1.acreage())
	fmt.Println(c2.acreage())
	fmt.Println(c3.acreage())

}
