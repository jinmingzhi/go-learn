package main

import "fmt"

func main() {
	type person struct {
		name string
		age  int
	}
	type Student struct {
		person // 匿名字段，那么默认Student就包含了person 的所有字段
		name   string
		class  string
	}

	jmz := Student{person{"jmz", 12}, "jly", "三年级二班"} // 调用方式较为特殊，契机
	fmt.Println(jmz)                                  // {{jmz 12} jly 三年级二班}
	fmt.Println(jmz.class)                            // 三年级二班
	fmt.Println(jmz.person.name)                      // jmz
	fmt.Println(jmz.name)                             // jly
}
