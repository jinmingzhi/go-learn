package main

import (
	"fmt"
	"reflect"
)

// 对象赋值给接口，

func main() {
	var v1 interface{}
	v1 = 6.7
	fmt.Println(v1, reflect.TypeOf(v1)) //6.7 float64
	vv := v1.(float64)
	fmt.Println(vv, reflect.TypeOf(vv)) //6.7 float64
	//fmt.Println(v1.(float32))  // 类型不正确就会报错

	fmt.Println(v1, reflect.TypeOf(v1)) // 6.78 float64

	var v2 interface{}
	v2 = 23
	fmt.Println(v2, reflect.TypeOf(v2)) //23 int
}
