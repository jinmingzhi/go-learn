package main

import (
	"fmt"
)

type inners struct {
	in1 int
	in2 int
}

type outer struct {
	b      int
	c      float32
	int    // 匿名字段。 // 在结构体中 对于 每一种数据类型只能一个匿名字段
	inners // 匿名字段.  内嵌结构类型
}

func main() {
	outer := new(outer)
	outer.in1 = 3
	outer.in1 = 4
	outer.b = 5
	outer.c = 5.0
	outer.int = 1
	fmt.Println(outer) //&{5 5 1 {4 0}}
}
