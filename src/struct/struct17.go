package main

import (
	"fmt"
)

func main() {
	people := []struct {
		age, height int
	}{
		{17, 178},
		{23, 168},
		{25, 189}}

	for _, v := range people {
		fmt.Println(v.height)
	}
}
