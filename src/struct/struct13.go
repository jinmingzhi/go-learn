package main

import (
	"fmt"
)

// 命名冲突
// 1、外层的覆盖了内层的
// 2、如果相同的名字在同一级别出现了两次，如果这个名字被程序使用了，将会引发一个错误（不使用没关系）。没有办法来解决这种问题引起的二义性，必须由程序员自己修正。
type A struct {
	a int
}
type B struct { // 继承了A
	A
	a int // 与 A的a冲突了怎么办???
	b int
}

type C struct {
	A
	c int
}

type D struct {
	c int
	d int
}

type E struct {
	C
	D
}

func main() {
	numberC := &C{A{1}, 2}
	fmt.Println(numberC.a) // numberC.A.a  1

	numberB := &B{A: A{a: 1}, a: 22, b: 333}
	fmt.Println(numberB.a) // 打印的是B里面的a

	numberE := &E{C{A{1}, 2}, D{3, 4}} // C 和 D 都有c
	fmt.Println(numberE)
	fmt.Println(numberE.d)
	// fmt.Println(numberE.c) //使用即报错
}
