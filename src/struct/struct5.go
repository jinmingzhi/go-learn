package main

import "fmt"

// 在go语言中，一类只需要实现了接口的所有的函数，我们就说这个类实现了该接口
// 意思是 go 没有明显的继承接口的，但是只要这个实现了接口的所有的函数，那么这个类

// 接口 定义
type Animal interface {
	Eat() bool
	Run() bool
}
type Dog struct {
}

func (dog *Dog) Eat() bool {
	fmt.Println("dog eating....")
	return true
}

func (dog *Dog) Run() bool {
	fmt.Println("dog running...")
	return true
}

func main() {
	var animal Animal
	dog := new(Dog)
	animal = dog
	animal.Run()
}
