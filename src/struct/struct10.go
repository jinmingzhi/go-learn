package main

import (
	"fmt"
	"strconv"
)

type number struct {
	f float64
}

func (self *number) String() string {
	return "number的值是:" + strconv.FormatFloat(self.f, 'f', -1, 64)
}

func main() {
	nr := &number{5.0}
	fmt.Println(nr) // 自动触发String 方法

}
