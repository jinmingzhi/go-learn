package main

import "fmt"

// 类的定义
type Integer struct {
	value int
}

// Integer 类的私有方法
func (a Integer) compare(b Integer) bool {
	return a.value < b.value
}

// 这边之所以要加 * ，是因为要引用原来的数据，并改变。如果不使用引用，就会是重复复制了一份数据，改变的也只是复制的数据，对原数据没有影响
func (a *Integer) Inc(num int) {
	fmt.Println(a) // &{3}
	a.value += num
}

func main() {
	a := Integer{3}
	//var a = Integer{3}
	b := Integer{4}

	fmt.Println(a.compare(b)) // true
	a.Inc(2)
	fmt.Println(a.value) // 5
	fmt.Println(a.compare(b))

}
