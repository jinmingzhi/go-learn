package main

import (
	"fmt"
)

// 继承
type person struct {
	school string
}

type teacher struct {
	person
	name string
}

func (t *teacher) teach() {
	fmt.Println(t.name, "正在教书中...")
}

type student struct {
	person
	name string
}

func (s *student) get_name() string {
	return s.name
}

// new 分配一个数据类型内存,内存的数据类型为零值,并返回指向该内存的指针.
// make 返回是一个引用,可初始化内存数据

func main() {
	// t := new(teacher) // 初始化零值。指针类型
	t := teacher{person: person{school: "上海"}, name: "李俊"} // 指名道姓的初始化值, 值类型
	// t := &teacher{person{"上海"}, "李俊"} //顺序初始化值, 指针类型
	// t.person.school = "上海"
	// t.name = "李俊"

	t.teach()
	// (*t).teach() // s上下的效果是一样的
}
