package main

import (
	"fmt"
	"reflect"
)

// 标签
type TwoInts struct {
	a int "this is a"
	b int "this is b"
}

func main() {
	tt := &TwoInts{a: 4, b: 5}
	for i := 0; i < 2; i++ {
		refTag(tt, i)
	}
}

func refTag(tt *TwoInts, ix int) {
	t := reflect.TypeOf(*tt)
	ixField := t.Field(ix)
	fmt.Println(ixField.Tag)
}
