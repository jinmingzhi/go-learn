package main

import (
	"fmt"
)

// 对象只支持封装，不支持继承和多态

type treeNode struct {
	value       int
	left, right *treeNode // left 和 right 都是treeNode 的引用类型
}

func creataNode(value int) *treeNode {
	return &treeNode{value: value}
}

func main() {
	var root treeNode
	fmt.Println(root) //{0 <nil> <nil>}
	root.value = 5
	root.left = &treeNode{value: 1} //意思是创建好 treeNode{value: 1}， 在将指针传递给root.left
	root.right = &treeNode{2, nil, nil}

	fmt.Println(root)               // {5 0xc04204c420 0xc04204c440}
	root.right.left = new(treeNode) //  给left 分配一个内存空间，是treeNode类型

	fmt.Println(root)            // {5 0xc04204c420 0xc04204c440}
	fmt.Println(root.right.left) // &{0 <nil> <nil>}

}
