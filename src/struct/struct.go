package main

import "fmt"

func main() {
	type person struct {
		name string
		age  int
	}
	jmz := person{"jmz", 12} // 只能在一行
	fmt.Println(jmz)
}
