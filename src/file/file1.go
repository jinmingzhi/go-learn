package main

import (
	"bufio"
	"fmt"
	"os"
)

// 文件读,行读
func main() {
	file, err := os.Open("abc.txt") // file  *os.File
	if err != nil {
		fmt.Printf("An error occurred on opening the inputfile\n" +
			"Does the file exist?\n" +
			"Have you got acces to it?\n")
		return
	}

	defer file.Close()
	fileReader := bufio.NewReader(file) //创建一个读取器，并将与 文件绑定
	for {
		line, err := fileReader.ReadString('\n')
		// ReadString(delim byte)，该方法从输入中读取内容，直到碰到 delim 指定的字符
		// 然后将读取到的内容连同 delim 字符一起放到缓冲区。

		if err != nil {
			break
		}
		fmt.Printf(line)
	}

}
