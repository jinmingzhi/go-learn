package main

import (
	"fmt"
	"io"
	"os"
)

// 文件拷贝
func main() {
	src, err := os.Open("jmz.txt") // 读取文件
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer src.Close()

	dst, err := os.OpenFile("jmz_copy.txt", os.O_WRONLY|os.O_CREATE, 0666) // 创建文件
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer dst.Close()
	io.Copy(dst, src)
}
