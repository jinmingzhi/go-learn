package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

// 整读整存。。 文件内容全部读取。。文件内容整体保存。。。不推荐
func main() {
	var inputtxt string = "abc.txt"
	var outputtxt string = "abc_copy.txt"

	buf, err := ioutil.ReadFile(inputtxt) // 直接读取全部内容，内容为[]byte
	if err != nil {
		fmt.Fprintf(os.Stdout, "file Error:%s\n", err)
		return
	}

	fmt.Println(buf)
	err = ioutil.WriteFile(outputtxt, buf, 0666)
	if err != nil {
		panic(err.Error())
	}

}
