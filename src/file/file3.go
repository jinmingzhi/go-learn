package main

import (
	"fmt"
	"os"
)

// 创建一个文件.简单的写入功能
func main() {
	file, err := os.OpenFile("jmz.txt", os.O_WRONLY|os.O_CREATE, 0666)

	if err != nil {
		fmt.Println(err.Error())
	}
	defer file.Close()
	file.WriteString("hello world\njmz\n")
}
