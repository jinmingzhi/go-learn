package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func main() {
	file, err := os.Open("abc.txt")
	check(err)
	defer file.Close()
	fileScanner := bufio.NewScanner(file) // 扫描器  主要作用是把数据流分割成一个个标记并除去它们之间的空格
	check(err)
	for fileScanner.Scan() {
		fmt.Println(fileScanner.Text())
	}
}
