package main

import (
	"bufio"
	"fmt"
	"os"
)

// 通过写入器写入文件
func main() {
	file, err := os.OpenFile("jly.txt", os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err.Error())
	}
	defer file.Close()

	outputWrite := bufio.NewWriter(file) // 创建一个写入器
	outputString := "hello world\n"
	for i := 0; i < 10; i++ {
		outputWrite.WriteString(outputString)
	}
	outputWrite.Flush()
}
