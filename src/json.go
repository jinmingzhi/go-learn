package main

import (
	"encoding/json" // 官方标准库 性能较差
	"fmt"
	// ffjson    //是上面的3-5倍
)

func main() {
	// 对json 进行 编码操作
	x := []int{1, 2, 3}

	s, err := json.Marshal(x)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(string(s))

	y := make(map[string]int)
	y["age"] = 23
	y["id"] = 34111141
	res, err := json.Marshal(y)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(string(res))

	type student struct {
		Name string `json:"student_name"` // 表示json 转变时，会将名称改为student_name
		age  int
	}

	jmz := student{"jmz", 23}
	res_obj, err := json.Marshal(jmz)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(string(res_obj)) // {"student_name":"jmz"}   // age为什么没有显示呢？go语言中小写开头，表示私有化，不为为人所查看

}
