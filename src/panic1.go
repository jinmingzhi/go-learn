package main

import (
	"fmt"
	"strconv"
	"strings"
)

type ParseError struct { //定义一个处理错误的结构体
	key   int
	Value string
	Err   error
}

func (p *ParseError) String() string { //给ParseError定义一个String()方法。
	return fmt.Sprintf(" [%q] type is not int!", p.Value)
}

func JudgmentType(fields []string) (numbers []int) { //这个函数是判断fields切片中的每个元素是否都可以转换成INT类型的。
	if len(fields) == 0 {
		panic("Nothing can be explained!")
	}
	for key, value := range fields {
		num, err := strconv.Atoi(value) //这里是讲每一个切片元素中的字符串进行转换。
		if err != nil {
			panic(&ParseError{key, value, err}) //如果解析出错就将自定义的ParseError结构体的error对象返回。
		}
		numbers = append(numbers, num) //如果转换成int类型没有出错的话就会被追加到一个提前定义好的切片中。
	}
	return //我们这里可以写numbers，说白了只要写一个[]int类型的都可以，当然，如果你不写的话默认就会返回我们提前定义好的numbers哟。
}

func StringParse(input string) (numbers []int, err error) { //这个函数是用来解析字符串的。
	defer func() { //用recover函数来接受panic抛出的错误信息。
		if ErrorOutput := recover(); ErrorOutput != nil {
			var ok bool
			err, ok = ErrorOutput.(error) //很显然，这里是一种断言操作，即判断是否有error类型出现。
			if !ok {
				err = fmt.Errorf("Parse error: %v", ErrorOutput)
			}
		}
	}()
	fields := strings.Fields(input)
	numbers = JudgmentType(fields)
	return
}

func main() {
	var yzj = []string{
		"100 200 300",
		"1 2 2.5 3",
		"30 * 40",
		"yinzhengjie Golang",
		"",
	}

	for _, ex := range yzj {
		fmt.Printf("正在解析[ %q]:\n ", ex)
		result, err := StringParse(ex)
		if err != nil {
			fmt.Println("解析结果：", err)
			continue
		}
		fmt.Println("解析结果：", result)
	}
}
