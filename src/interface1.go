package main

import "fmt"

// type-switch 判断接口类型。不可用fallthrough
func main() {
	var a interface{}
	a = 12
	switch a.(type) {
	case int: // true
		fmt.Println("a 是int型")
	case string:
		fmt.Println("a 是string")
	default:
		fmt.Println("不知道什么类型")
	}
}
