package main

import "fmt"

var (
	firstName, lastName, s string
	i                      int
	f                      float32
	s1, s2                 string
	input                  = "56.12 / 5212 / Go"
	format                 = "%f / %d / %s"
)

func main() {
	fmt.Println("Please enter your full name: ")
	fmt.Scanln(&firstName, &lastName) // 已空格隔开
	// fmt.Scanf("%s %s", &firstName, &lastName)
	fmt.Printf("Hi %s and %s!\n", firstName, lastName) // Hi Chris Naegels
	fmt.Sscanf(input, format, &f, &i, &s)
	fmt.Println("From the string we read: ", f, i, s)
	// 输出结果: From the string we read: 56.12 5212 Go
	fmt.Sscanf(" jmz and aaa", "%s and %s", &s1, &s2)
	fmt.Println("test:", s1, s2) //test: jmz  aaa
}
