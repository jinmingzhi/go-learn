package main

import (
	"fmt"
)

// 函数可以当做参数传递
func count(op func(a, b int) int, a, b int) (num int, err error) {
	return op(a, b), nil
}

func sum(a, b int) int {
	return a + b
}

// 接受一大串int 放置nums 中
func summation(nums ...int) int {
	sum := 0
	fmt.Println(nums)
	for _, num := range nums {
		sum += num
	}
	return sum
}

func main() {

	if num, err := count(func(a, b int) int {
		return a * b
	}, 3, 4); err != nil {
		panic(err)
	} else {
		fmt.Println(num)
	}

	num1, err := count(sum, 2, 3)
	if err != nil {
		panic(err)
	} else {
		fmt.Println(num1)
	}

	fmt.Println(summation(1, 2, 3, 4, 5))

}
