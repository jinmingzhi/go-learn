package main

import (
	"fmt"
	"net"
	"reflect"
	"strings"
)

func main() {
	server, err := net.Listen("tcp", "127.0.0.1:8888")
	fmt.Println(reflect.TypeOf(server))
	if err != nil {
		panic("tcp listen wrong")
	}
	defer server.Close()

	for {
		conn, err := server.Accept()
		if err != nil {
			panic("Error acception" + err.Error())
		}
		go doServerStuff(conn)
	}
}

func doServerStuff(conn net.Conn) {
	defer conn.Close()
	for {
		buf := make([]byte, 512)
		len, err := conn.Read(buf)
		if err != nil {
			panic("Error reading" + err.Error())
			return
		}
		s := strings.ToUpper(string(buf[:len]))
		conn.Write([]byte(s))
	}
}
