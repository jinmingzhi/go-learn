package main

import (
	"fmt"
)

func main() {
	str := "jmz"
	// str[0] = "J" // 会报错   cannot assign to str[0]

	c := []byte(str)
	c[0] = 'J' // Go语言的单引号一般用来表示「rune literal」   // 只能 1bytes
	str = string(c)
	fmt.Println(str)
}
