package main

import (
	"fmt"
	"strings"
)

func main() {
	// 去除字符串   cmd 查看
	// strings.Trim(s, cutset)
	// strings.TrimSpace(s, cutset)
	// strings.TrimLeft(s, cutset)
	// strings.TrimRight(s, cutset)
	// strings.TrimPrefix(s, prefix)
	// strings.TrimSuffix(s, suffix)

	s := "      aabaaa   baaab   aabb    "
	str := "aaabbaaa"
	fmt.Println("s:", s)

	s1 := strings.TrimSpace(s)
	fmt.Println(s1, len(s1)) //aabaaa   baaab   aabb 21

	fmt.Println("str:", str) // aaabbaaa

	s2 := strings.Trim(str, "a")
	fmt.Println(s2) //bb

	s3 := strings.TrimLeft(str, "a")
	fmt.Println(s3) // bbaaa

	s4 := strings.TrimRight(str, "a")
	fmt.Println(s4) // aaabb

	s5 := strings.TrimPrefix(str, "a")
	fmt.Println(s5) // aabbaaa

	s6 := strings.TrimSuffix(str, "a")
	fmt.Println(s6) //aaabbaa

	/*
		// bug
		cfgs := "mongodb://off"
		cfgs = strings.TrimLeft(cfgs, "mongodb:/")
		fmt.Println(cfgs)
		// ff
	*/
}
