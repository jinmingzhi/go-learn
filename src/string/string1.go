package main

import (
	"fmt"
	"strings"
)

func main() {
	s := "jmz is cool"
	// strings.HasPrefix(s, prefix string) bool
	b := strings.HasPrefix(s, "jmz") // 是否已prefix大头
	fmt.Println(b)

	// strings.HasSuffix(s, suffix string) bool   // 判断 s 是否适宜suffix结尾
	b1 := strings.HasSuffix(s, "ol") // 是否已suffix 结尾
	fmt.Println(b1)

	// strings.Contains(s, substr string) bool   // 判断s 是否包 substr字符
	c1 := strings.Contains(s, "co")
	fmt.Println(c1)

	// strings.Index(s, str string) int  // 返回字符s 的下标
	x1 := strings.Index(s, "m") // -1 表示没有
	fmt.Println(x1)

	// strings.LastIndex(s, str string) int  //返回字符最后出现的下标索引
	x2 := strings.LastIndex(s, "o") // -1 表示没有
	fmt.Println(x2)

	//strings.Replace(str, old, new, n) string  // 将old 替换 new 替换n次，返回一个新的字符串
	r1 := strings.Replace(s, "j", "aaaa", -1)
	fmt.Println(r1)

	// strings.Count(s, str string) int     // str 在s 中出现的次数
	c := strings.Count(s, "o")
	fmt.Println(c)

	// strings.Repeat(s, count) s   // 将s 重复count次 并返回新的字符串
	new_s := strings.Repeat(s, 3)
	fmt.Println(new_s)

	// 大小写转化
	// strings.ToUpper(s) string
	// strings.ToLower(s) string
	fmt.Println(strings.ToUpper(s))
	fmt.Println(strings.ToLower(s))

}
