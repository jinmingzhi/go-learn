package main

import (
	"fmt"
	"reflect"
	"strings"
)

func main() {
	// 分割字符串
	// strings.Fields(s) []string
	arr := strings.Fields(" a   bb   cc  dd fffff")
	fmt.Println(arr, reflect.TypeOf(arr))

	// strinfs.Split(s, sep) []string
	arr1 := strings.Split("this|is|shanghai", "|")
	fmt.Println(arr1)

	// 字符串 拼接
	// strings.Join([]strings, sep) string
	s1 := strings.Join(strings.Split("shanghai|上海|北京", "|"), " ")
	fmt.Println(s1)
}
