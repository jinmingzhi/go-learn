package main

import (
	"fmt"
)

func unsignLenofString(s string) int {
	m := make(map[rune]int)
	for _, v := range []rune(s) {
		m[v] = 1
	}
	return len(m)
}

func main() {
	fmt.Println(unsignLenofString("a我是bbbbbbcc"))
	fmt.Println(unsignLenofString("aaabbbccc"))
	fmt.Println(unsignLenofString(""))
	fmt.Println(unsignLenofString("一二三二一"))
}
