package main

import (
	"fmt"
)

// 判断 map 中key的值是否存在
// value, ok := map[key]
// value map对应键的值
// ok bool ，存在未true 不存在为false
func main() {
	var m1 = map[string]float32{
		"C0": 16.35, "D0": 18.35, "E0": 20.60, "F0": 21.83,
		"G0": 24.50, "A0": 27.50, "B0": 30.87, "A4": 440}

	if v, ok := m1["C0"]; ok {
		fmt.Println("map 存在key 的值为：", v)
	} else {
		panic("map 中不存在key的值")
	}

}
