package main

import (
	"fmt"
)

// for-range
// map 是一种特殊的数据结构：一种元素对（pair）的无序集合
func main() {
	var m map[int]float32
	m = make(map[int]float32)
	m[1] = 1.0
	m[2] = 2.0
	m[3] = 3.0
	m[4] = 4.0

	// key（键）   value（值）
	for key, value := range m {
		fmt.Printf("m 的Key:%d,value:%f\n", key, value)
	}

	fmt.Println("下面只显示value")
	// 忽略键，只需要值
	for _, v := range m {
		fmt.Println(v)
	}

	fmt.Println("下面只显示key")
	// 只需要key
	for k := range m {
		fmt.Println(k)
	}
}
