package main

import (
	"fmt"
)

// delete 删除map中的key-value
// key 不存在也可以改操作也不会报错
func main() {
	var m = map[string]float32{"height": 1.74}
	delete(m, "height")
	delete(m, "width")
	fmt.Println(m)
}
