package main

import (
	"fmt"
)

// 这里对调是指调换 key 和 value。如果 map 的值类型可以作为 key 且所有的 value 是唯一的，
// 那么通过下面的方法可以简单的做到键值对调。

var (
	barVal = map[string]int{"alpha": 34, "bravo": 56, "charlie": 23,
		"delta": 87, "echo": 56, "foxtrot": 12,
		"golf": 34, "hotel": 16, "indio": 87,
		"juliet": 65, "kili": 43, "lima": 98}
)

func main() {
	var m map[int]string = make(map[int]string)
	for k, v := range barVal {
		m[v] = k
	}
	fmt.Println(m)
}
