package main

import (
	"fmt"
)

//不要使用 new，永远用 make 来构造 map
func main() {
	// 通过new 分配一个应用对象
	// m := new(map[string]float32)
	// m["height"] = 1.33 // invalid operation: m["height"] (type *map[string]float32 does not support indexing)
	// fmt.Println(m)

	m1 := make(map[string]float32)
	m1["height"] = 1.33
	fmt.Println(m1)

}
