package main

import (
	"fmt"
)

/*
	什么字段类型可以错误key
	1、map 使用hash 表
	2、 除了slice,map,func的内建类型都可以作为key
	3、Struct 类型不包含上述字段，也可以作为key
*/
func main() {
	m1 := make(map[string]string) // 申明 创建一个非nil的map
	m1["name"] = "jmz"
	m1["name"] = "aaa"
	fmt.Println(m1)
}
