package main

import (
	"fmt"
	"sort"
)

// map 默认是无序的，不管是按照 key 还是按照 value 默认都不排序
// 如果你想要为map排序，需要将keycopy到一个切片中，然后可以通过切片的for-range打印出所有的key-value

var (
	barVal = map[string]int{"alpha": 34, "bravo": 56, "charlie": 23,
		"delta": 87, "echo": 56, "foxtrot": 12,
		"golf": 34, "hotel": 16, "indio": 87,
		"juliet": 65, "kili": 43, "lima": 98}
)

func main() {
	keys := make([]string, len(barVal))
	i := 0
	for k := range barVal {
		keys[i] = k
		i++
	}

	fmt.Println(barVal)
	sort.Strings(keys)
	for _, key := range keys {
		fmt.Println(key, barVal[key])
	}

}
