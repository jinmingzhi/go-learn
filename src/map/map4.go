package main

import (
	"fmt"
)

// map 是一种特殊的数据结构：一种元素对（pair）的无序集合
func main() {
	// 制作map
	// var m1 = make(map[string]string)
	m1 := make(map[string]string)
	m1["name1"] = "aaa"
	m1["name2"] = "bbb"

	var m2 map[string]int
	m2 = map[string]int{"age1": 12, "age2": 16}
	m2["age3"] = 33

	fmt.Println(m1)         // map[name1:aaa name2:bbb]
	fmt.Println(m2)         // map[age2:16 age3:33 age1:12]
	fmt.Println(m1["name"]) //
	fmt.Println(m2["age4"]) //0

	// 如果 map 中没有 key1 存在，那么 v 将被赋值为 map1 的值类型的空值。
}
