package main

import (
	"fmt"
)

func main() {
	mf := map[int]func() int{
		1: func() int { return 10 },
		2: func() int { return 20 },
		5: func() int { return 50 },
	}
	fmt.Println(mf)

	mp1 := make(map[int][]int) // 用切片来做map的值
	mp2 := make(map[int]*[]int)
	fmt.Println(mp1, mp2)
}
