package main

import(
	"fmt"
	"reflect"
)

func main(){
	m := map[string]string{
		"name":"jmz",
		"age":"24",
	}

	m1 := make(map[string]string)   // m1 == empty map

	var m2 map[string]string  		// m2 == nill
									// nil empty map 可以共用

	if m2 == nil{
		fmt.Println("m2 is nill")
	}

	fmt.Println(m)
	fmt.Println(m1)
	fmt.Println(m2)

	for k,v := range m {   // map 无序
		fmt.Println(k, v)
	}

	// map 在没有key 时，任然可是有值，只是是空字符串
	// 在查找key时，有则会返回有true，无则false
	name,ok := m["name"]
	fmt.Println(name,ok)  // jmz true

	if school,ok := m["scool"];ok{
		fmt.Println(school)
	} else{
		fmt.Println("school 不存在是空的,且是string类型",school,reflect.TypeOf(school))
	}

	delete(m,"name")
	fmt.Println(m)  //map[age:24]
	delete(m,"name")  // 删除不存在的key不会报错
	fmt.Println(m)


	fmt.Println(len(m))  // 1

}
