package main

import "fmt"

// map 是一种特殊的数据结构：一种元素对（pair）的无序集合
func main() {
	/* make 用于内建类型（map,slice和channel） 的内存分配 */

	//var student map[string]float32
	//student = make(map[string]float32)  // map 一定要申请内存空间才可以，数组也一样，只是底层做了这个事而已
	student := make(map[string]float32) // 简短申明
	student["jmz"] = 1.75
	student["qqc"] = 1.65
	student["aaa"] = 1.45
	fmt.Println(student)
	// map[jmz:1.75]
}
