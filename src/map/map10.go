package main

import (
	"fmt"
)

// map 类型的切片
// 假设我们想获取一个map类型的切片，我们必须使用两次make() 函数，第一次分配切片，第二次分配切片中的每一个map元素
func main() {
	items1 := make([]map[int]float32, 5)
	for i := range items1 {
		items1[i] = make(map[int]float32)
		items1[i][1] = 1.0
	}
	fmt.Println(items1) // [map[1:1] map[1:1] map[1:1] map[1:1] map[1:1]]

	//
	items2 := make([]map[int]float32, 5)
	for _, item := range items2 { // item is only a copy of the slice element.item 只是copy了这个切片的元素
		item = make(map[int]float32) // This 'item' will be lost on the next iteration., 这个item 会在下一次迭代中消失
		item[1] = 1.0
	}
	fmt.Println(items2) // [map[] map[] map[] map[] map[]]
}

// 需要注意的是，应当像 A 版本那样通过索引使用切片的 map 元素。在 B 版本中获得的项只是 map 值的一个拷贝而已，所以真正的 map 元素没有得到初始化。
