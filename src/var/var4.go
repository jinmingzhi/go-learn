package main

import (
	"fmt"
)

func change(i *int) {
	*i = 4
}

func main() {

	// 普通的值变量，一出手就有一个内存和内存所存在的内存地址

	var a int = 5
	var b int = 6
	fmt.Println(&a, &b) // 0xc042052080 0xc042052088
	a = 4
	b = 22
	fmt.Println(&a, &b) // 0xc042052080 0xc042052088

}
