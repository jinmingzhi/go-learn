package main

import (
	"fmt"
)

func change(i *int) {
	*i = 4
}

func main() {
	var i *int
	var a int = 8
	var b int = 4
	i = &a
	fmt.Println(&a, i) // 0xc042052080 0xc042052080
	i = &b
	fmt.Println(&a, i) //0xc042052080 0xc042052088

}
