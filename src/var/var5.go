package main

import (
	"fmt"
)

func change(i *int) {
	*i = 4
}

func main() {
	var i *int
	var a int = 8
	i = &a
	fmt.Println(&a, i)
	a = 44
	fmt.Println(&a, i, a)
}
