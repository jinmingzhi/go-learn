package main

import "fmt"

// 变量类型 一定是放在在变量后面的
var z int = 12

//d := "jmz"   //这样申明不可用于函数体外部
const PI float32 = 3.14 // go 商量定义
func main() {
	var x int
	x = 1
	// 简短申明   不能在函数体外使用
	y := "hello world"
	fmt.Printf("%d:%s:%d \n", x, y, z)

	fmt.Println(PI)

}
