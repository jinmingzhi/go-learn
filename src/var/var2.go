package main

import (
	"fmt"
)

func change(i *int) {
	*i = 4
}

func main() {

	// 普通的值变量，一出手就有一个内存和内存所存在的内存地址

	var a int = 5
	b := a              // 这个是重新拷贝了一份
	fmt.Println(&a, &b) // 0xc04200c0c8 0xc04200c0e0
	a = 4
	fmt.Println(b)
	fmt.Println(&a, &b) // 0xc04200c0c8 0xc04200c0e0

}
