package main

import (
	"fmt"
	"reflect"
)

// 单引号 双引号 和 反引号
func main() {
	str := "我爱中国，china"

	str1 := 'd' // str1 := 'dsadsada'  不可以，只能是1bytes

	str2 := `
		dsadsa
		dsa
		dadsa
		dsad
	`

	fmt.Println(str, reflect.TypeOf(str))   // 我爱中国，china string
	fmt.Println(str1, reflect.TypeOf(str1)) // 100 int32
	fmt.Println(str2, reflect.TypeOf(str2))
	/*

			dsadsa
			dsa
			dadsa
			dsad
		 string


	*/

}
