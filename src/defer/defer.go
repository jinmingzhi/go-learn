package main

import "fmt"

// defer 关键字 ，程序运行结束之前，运行defer 后的程序.  一般是资源的清理工作
// file.Open("file")
// defer file.Close()

func main() {
	for i := 0; i < 5; i++ {
		defer fmt.Println(i) // 程序运行结束前会执行，执行 先进后出 原则
	}
	fmt.Println("我会在循环之前出现")
	//我会在循环之前出现
	//4
	//3
	//2
	//1
	//0

}
