package main

import (
	"fmt"
)

/**
 * defer 会在当前的函数体内 return之后触发,或者说是函数结束之后触发
 *
 */

func test() (ret int) {
	defer fmt.Println("this is func test")
	ret = 1
	return ret
}

func test1() {
	defer fmt.Println("this is func test1")
	fmt.Println("test1")
}

func main() {
	res := test()
	test1()
	fmt.Println(res)
}

/*

this is func test
test1
this is func test1
1



*/
