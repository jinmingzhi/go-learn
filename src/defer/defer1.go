package main

import "fmt"

// 匿名函数的运用场景

func main() {
	defer func() {
		fmt.Println("我会在程序运行结束前运行，做收尾工作")
	}() // 函数定义后，需要调用执行，所以直接在函数定义后加上() 调用。其实就是下面的f()
	/*
		//就相当于
		f := func(){
			fmt.Println("我会在程序运行结束前运行，做收尾工作")
		}
		defer f()
	*/
}
