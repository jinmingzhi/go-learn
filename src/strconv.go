package main

import (
	"fmt"
	"strconv"
)

// IntSize      操作系统的位数
// Atoi		    字符串转int
// Itoa			int 转字符串
// ParseInt		字符串转int
// ParseFloat	字符串转float
// FormatFloat  float 转字符串
// FormatInt	int 转字符串

func main() {
	// IntSize 用了获取 操作系统int 类型所占的位数
	fmt.Println("The size of ints is", strconv.IntSize)

	fmt.Println("--------string to int---------")
	s := "12"
	i, err := strconv.Atoi(s)
	if err != nil {
		panic("this is string error:")
	} else {
		fmt.Println(i)
	}
	fmt.Println("----------- string  to flaot------------")
	f, err := strconv.ParseFloat("12", 64)
	if err != nil {
		panic("this is string to float errr")
	} else {
		fmt.Println(f)
	}

	fmt.Println("-----------int to string------------")

	s = strconv.Itoa(13) // 因为严格限制必须是int型，所以一定可以转成string类
	fmt.Println(s)

	fmt.Println("-----------float to string------------")
	s1 := strconv.FormatFloat(4.3333, 'f', -1, 64)
	// flaot
	// fmt 格式（'b','e','f','g'）
	// prec 进度，-1 全部
	// bitSize  32表示float32 64 表示float64
	fmt.Println(s1)

	fmt.Println("-----------string to int------------")
	s2, _ := strconv.ParseInt("22", 10, 64) // base 表示几进制
	fmt.Println(s2)

	fmt.Println("-----------int to string------------")
	i2 := strconv.FormatInt(44, 10) // base 表示几进制，10 表示10几进制
	fmt.Println(i2)

}
