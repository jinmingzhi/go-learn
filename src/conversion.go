package main

import (
	"fmt"
)

func main() {
	// int float
	// string
	// bool

	var i = 12
	i_f := float64(i)
	fmt.Println(i_f)

	var f float32 = 3.4
	fmt.Println(int64(f))

	var a int32 = 4
	fmt.Print(float64(a))

	//var x string =  "4"
	//fmt.Println(int(x))  // string 转会报错的

}
