package main

import (
	"fmt"
)

func main() {
	x := 1
	switch x {
	case 1:
		fmt.Println("是1") // case 后面没有break,因为go语言优化了这部分，在编译过程中自动补全了这个
	case 2:
		fallthrough // 直接跳过这个部分
	case 3:
		fmt.Println("是2或3")
	default:
		fmt.Println("是4")
	}

	var name interface{} // 这是interface 类型
	name = "jmz"
	// 直接使用name:="jmz"  不可以，可能是和inteface 有关。。
	switch name.(type) { // interface 类型可以使用   变量.(type)   只能在switch 中使用
	case string:
		fmt.Println("我是string")
	case int:
		fmt.Println("我是int")
	case float32:
		fmt.Println("我是float32")
	default:
		fmt.Println("其他类型")
	}

	age := 24
	if age == 24 {
		fmt.Println("正确")
	} else if age < 24 {
		fmt.Println("年龄小了")
	} else {
		fmt.Println("年龄大了")
	}

	var v interface{} // interface 类型，    该类型可以使用   变量.(type)
	v = 1.34
	if v1, ok := v.(float64); ok { // 类型查询， 表示v是否是float64,将数据和结果给v1,ok,ok如果是true则执行项目代码
		fmt.Println(v1, ok) // 1.34 true
	} else {
		fmt.Println("v不是float64 的数据类型")
	}

	/*
		// 这种方式无法使用，只能使用 var b  interface{}
		b := 2.34
		if b1,ok := b.(float32);ok{
			fmt.Println(b1,"b是float64")
		}
	*/

	names := make(map[string]int)
	names["jmz"] = 13 // 单引号和双引号好像有区别
	names["jly"] = 23
	for i, v := range names {
		fmt.Println(i, v)
	}

	y := 1
	for y <= 100 {
		fmt.Println(y)
		y += 1
	}

	/*
		// 这是无限循环
		for {
			fmt.Println("y")
		}
	*/

}
