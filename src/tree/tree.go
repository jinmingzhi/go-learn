package tree

import (
	"fmt"
)

type tree struct { // 小写开头,外部无法引用
	name string
}

func (t *tree) Photosynthesis() {
	fmt.Printf("%s树木正在进行光合作用\n", t.name)
}

func Instant(name string) *tree { // 控制实例
	return &tree{name: name}
}
