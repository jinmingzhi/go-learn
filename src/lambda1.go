package main

import (
	"fmt"
	"time"
)

func main() {
	// 程序终止前执行
	defer func() {
		fmt.Println("我会在最后执行的")
	}()

	for i := 0; i < 10; i++ {
		go func(a int) {
			fmt.Println(a)
		}(i)
	}

	time.Sleep(1 * time.Second)

}
