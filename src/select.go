package main

import (
	"fmt"
)

// select
func main() {
	ch := make(chan int)
	for i := 0; i < 10; i++ {
		go func(ch chan int) {
			ch <- 1
		}(ch)
	}

	select {
	case <-ch:
		fmt.Println("comr to read cn!")
	case ch <- 1:
		fmt.Println("aaa")
	default:
		fmt.Println("comt ti default")
	}

}
