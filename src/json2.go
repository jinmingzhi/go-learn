package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	//对数组,切片
	s := []string{"j", "m", "z"}
	data, err := json.Marshal(s)
	// func Marshal(v interface{})(data []byte,err error)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(string(data))

	// 对集合
	m := map[string]float64{"jmz": 1.75, "jyl": 1.64}
	data, err = json.Marshal(m)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(string(data))

	// 对结构体
	type people struct {
		Name  string `json:"name"` // 内部必须大写,才能被外部使用,json就是这样.   后面是起别名
		Age   int
		Hobby []string
	}

	p := people{Name: "jmz", Age: 24, Hobby: []string{"看书", "电影"}} // 内部内容一顶要大写
	data, err = json.Marshal(p)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(string(data))
	//{"name":"jmz","Age":24,"Hobby":["看书","电影"]}

}
