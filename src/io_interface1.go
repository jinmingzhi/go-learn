package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	// unbuffered
	fmt.Fprintf(os.Stdout, "%s\n", "hello world! - unbuffered") // 还没有输出，只是告知要想终端输出
	// and now so does buf.
	fmt.Fprintf(os.Stdout, "%s\n", "hello world! - buffered")
	// buffered: os.Stdout implements io.Writer
	buf := bufio.NewWriter(os.Stdout)
	buf.Flush() // 最后统一输出
}
