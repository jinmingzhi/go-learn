package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func checkErr(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println(r)
		}
	}()
	client, err := net.Dial("tcp", "127.0.0.1:51345")
	checkErr(err)
	defer client.Close()
	go read(client)
	for {
		input := bufio.NewScanner(os.Stdin)
		input.Scan()
		client.Write([]byte(input.Text()))
	}
}

func read(conn net.Conn) {
	defer conn.Close()
	for {
		b := make([]byte, 512)
		length, err := conn.Read(b)
		checkErr(err)
		fmt.Println(string(b[:length]))
	}
}
