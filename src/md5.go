package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	// "strconv"
)

func echo(i interface{}) {
	fmt.Printf("(%v ,%T)\n", i, i)
}

func main() {
	Md5Int := md5.New()
	Md5Int.Write([]byte("zhangsan"))
	result := Md5Int.Sum([]byte("jmz")) //6a6d7a01d7f40760960e7bd9443513f22ab9af   向前追加的意思
	// result := Md5Int.Sum([]byte("")) //01d7f40760960e7bd9443513f22ab9af
	echo(result) // ([106 109 122 1 215 244 7 96 150 14 123 217 68 53 19 242 42 185 175] ,[]uint8)

	fmt.Println(hex.EncodeToString(result[:])) // 6a6d7a01d7f40760960e7bd9443513f22ab9af
	fmt.Printf("%x", result)
}
