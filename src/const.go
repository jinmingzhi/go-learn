package main

import (
	"fmt"
	"reflect"
)

func my_len(a string) int {
	return len(a)
}

const a, b, c = "jmz", 12, "cc"
const (
	name     string = "jmz"      // 显示定义
	age             = 12         // 隐式定义
	name_len        = len("jmz") // 常量的值只运行go的内置函数的
)

//const my_name_len = my_len("jmz") //const initializer my_len("jmz") is not a constant

var my_name_len = my_len("jmz") // 变量可以使用 自定义函数作为值

var x, y, z = "xx", "yy", 21

//var x,y string,z = "xx","yy",21  //报错，不可以怎么定义，类型智能放在最后

func main() {
	var v_name_len = len("aaa")
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(z)
	fmt.Println(name)
	fmt.Println(age)
	fmt.Println(reflect.TypeOf(age)) // int
	fmt.Println(name_len)
	fmt.Println(v_name_len)
}
