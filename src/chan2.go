package main

import (
	"fmt"
	"time"
)

// 用于获取chan内的值
// <-chan  表示只获取通道内的值
func Worker(c <-chan int) {
	for i := range c {
		fmt.Println(i)
	}
}

// chan 工厂方法，用于创建chan,同时返回只获取值的chan
// chan<- 表示只接收通道内的值
func createWorker() chan<- int {
	c := make(chan int, 3)
	go Worker(c)
	return c
}

// 其实收应该在主协程（没有主协程的概念，最外层的协程）中获取数据，这样就能够保证，数据接受完。
func main() {
	c := createWorker()
	c <- 1
	c <- 2
	c <- 3
	c <- 4
	close(c)
	time.Sleep(time.Millisecond)
}
