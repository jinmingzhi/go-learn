package main

import (
	"fmt"
)

// 将函数作为参数转递

func count(a, b int, f func(a, b int) int) int {
	return f(a, b)
}

func sum(a, b int) int {
	return a + b
}

func main() {
	ret := count(2, 3, sum)
	fmt.Println(ret)
}
