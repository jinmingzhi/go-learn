package main

import (
	"fmt"
	"strings"
)

func main() {
	addJpb := MakeAddSuffix(".jpg")
	addAvi := MakeAddSuffix(".avi")
	fmt.Println(addJpb("上海"))
	fmt.Println(addAvi("上海"))
}

func MakeAddSuffix(suffix string) func(string) string {
	return func(name string) string {
		if !strings.HasSuffix(name, suffix) {
			return name + suffix
		}
		return name
	}
}
