package main

import "fmt"

// 传值
func sum(a, b int) int {
	return a + b
}

// 传引用
func getsum(a *int) {
	*a = *a + 1
}

//  传引用在类 那边有点不一样
func jia2(a *int) {
	*a += 2
}

func main() {
	fmt.Println(sum(1, 2))
	x := 1
	getsum(&x)
	fmt.Println(x)

	y := 3
	jia2(&y)
	fmt.Println(y) //5

}
