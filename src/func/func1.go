package main

import "fmt"

//匿名函数

func main() {
	//这是匿名函数，但这种用法和普通函数定义又有什么区别，所以，匿名函数不是怎么用的。但要记住有怎么可匿名函数
	f := func(a, b int) int {
		return a + b
	}

	fmt.Println(f(2, 3))
}
