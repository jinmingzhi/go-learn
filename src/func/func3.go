package main

import (
	"fmt"
)

// 变长参数传递
func sum(i ...int) (ret int) {
	for d := range i {
		ret += d
	}
	return
}

func main() {
	res := sum(1, 2, 3, 4, 5)
	fmt.Println(res)
}
