package main

import (
	"fmt"
)

func printdec(i int) {
	if i <= 0 {
		return
	}
	fmt.Println(i)
	printdec(i - 1)
}

func main() {
	printdec(10)
}
