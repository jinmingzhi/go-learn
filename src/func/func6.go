package main

import (
	"fmt"
)

func main() {
	f := adder()
	fmt.Println(f(3))
}

// 将函数作为返回值
func adder() func(b int) int {
	return func(b int) int {
		return b + 2
	}
}
