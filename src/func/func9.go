package main

import (
	"fmt"
	"time"
)

// 计算函数执行了多少商家
func main() {
	start := time.Now()
	fmt.Println(sum(1, 2, 3, 4, 5, 6))
	end := time.Now()
	delta := end.Sub(start)
	fmt.Println(delta)
}

func sum(ints ...int) (ret int) {
	for _, i := range ints {
		ret += i
	}
	return
}
