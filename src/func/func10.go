package main

import (
	"fmt"
	"time"
)

// 通过内存	缓存提升性能
// 避免重复计算
const LIM = 41

var fibs [LIM]int

func main() {
	start := time.Now()
	for i := 0; i < LIM; i++ {
		fmt.Printf("fibonacci(%d) is: %d\n", i, fibonacci(i))
	}
	end := time.Now()
	delta := end.Sub(start)
	fmt.Printf("longCalculation took this amount of time: %s\n", delta)
}

func fibonacci(n int) (ret int) {
	if fibs[n] != 0 {
		ret = fibs[n]
		return
	}

	if n <= 1 {
		ret = 1
	} else {
		ret = fibonacci(n-1) + fibonacci(n-2)
	}
	fibs[n] = ret
	return
}
