package main

import (
	"fmt"
)

func main() {
	f()
}

// 闭包函数
func f() {
	g := func(i int) {
		fmt.Println(i)
	}
	for i := 0; i < 4; i++ {
		g(i)
	}

}
