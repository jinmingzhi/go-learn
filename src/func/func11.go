package main

import (
	"fmt"
)

// go 的闭包1
func sumFun() func(i int) int {
	var sum int
	return func(v int) int {
		sum = sum + v
		return sum
	}
}

func main() {
	sum := sumFun()
	for i := 0; i < 50; i++ {
		s := sum(i)
		fmt.Println(s)
	}
}
