package main

import (
	"fmt"
)

func main() {
	var ok bool                // 默认false
	var s string               //  空
	var i int                  // o
	var f float32              // 0
	var e error                // <nil>
	var stuct struct{}         // {}   空结构体
	var interface1 interface{} // <nil>

	fmt.Println(ok)
	fmt.Println(s, len(s)) // kong 0
	fmt.Println(i)
	fmt.Println(f)
	fmt.Println(e)
	fmt.Println(stuct)
	fmt.Println(interface1)
}
