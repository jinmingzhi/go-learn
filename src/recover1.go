package main

import (
	"fmt"
)

// recover 只能在defer 中使用.用于取得 panic 调用中传递过来的错误值。程序代码则无法调用
// 只能获取panic 自定义的错误数据。无法获取程序中的错误数据
func main() {
	fmt.Println("--------start---------")
	test()
	fmt.Println("--------end---------")
}

func test() {
	defer func() {
		if e := recover(); e != nil {
			fmt.Printf("Panicing %s\r\n", e)
		}
	}()
	panic("aaaa")
}
