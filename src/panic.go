package main

import "fmt"

//捕获异常   panic 和recover

func main() {
	defer fmt.Println("报错也不会影响到我的运行") // 我会在程序程序结束前执行
	panic("我是主动抛出异常")
}
