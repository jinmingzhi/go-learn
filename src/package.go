package main

//包只会被导入一次
// 深度优先导入
import (
	_ "animal" // 导入该包，但不导入整个包，只是执行了init方法，因此而发通过包名调用包内的函数和全局变量
	"fmt"
	"hello"
	"member"
	M_school "member/school" // 起别名
)

func main() {
	fmt.Println(hello.Hello)
	fmt.Println(hello.Sum(1, 2))
	fmt.Println(M_school.Name)
	fmt.Println(member.School)
	//fmt.Println(animal.Animal)
	//fmt.Println(animal.Eat())

	/*
	导入包就会执行我，对吗？
	我在导入执行
	jmz
	3
	上海交大
	上海交大
	*/
}
