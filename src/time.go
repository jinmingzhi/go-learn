package main

import (
	"fmt"
	"reflect"
	"time"
)

func main() {
	t := time.Now()
	fmt.Println(t, reflect.TypeOf(t))
	fmt.Printf("%v-%v-%v %v:%v:%v", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
	fmt.Println(t.Day())
	fmt.Println(t.Month())
	fmt.Println(t.Year())
	fmt.Println(t.Hour())
	fmt.Println(t.Minute())
	fmt.Println(t.Second())

	fmt.Println(time.Now().Unix())                        // 时间戳  1535529205
	fmt.Println(time.Now().Format("2006-01-02 15:04:05")) // 格式化时间

	// 时间戳 转格式化
	fmt.Println(time.Unix(1535529205, 0).Format("2006-01-02 15:04:05")) // 2018-08-29 15:53:25
	// 格式化转时间戳
	time_unix, err := time.Parse("2006-01-02 15:04:05", "2018-08-29 15:53:25")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(time_unix.Unix()) //1535558005
	}

	fmt.Println("time start....")
	time.Sleep(1 * time.Second)
	fmt.Println("time after....")
}
