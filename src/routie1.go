package main

import (
	"fmt"
	"time"
)

func Sum(a, b int) {
	fmt.Println(a + b)
}

func main() {
	for i := 1; i < 10; i++ {
		go Sum(i, i)
	}
	time.Sleep(1000000000) // 1000000000
	fmt.Println("完成了")

}
