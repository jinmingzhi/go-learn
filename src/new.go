package main

import (
	"fmt"
)

// new 分配一个数据类型内存,内存的数据类型为零值,并返回指向该内存的指针.
// make 返回是一个引用,可初始化内存数据

func main() {
	var i *int   // 只是声明i是int引用类型, 不会分配空间
	i = new(int) //给i 分配一个int的内存空间
	*i = 100
	fmt.Println(*i)

	// 可以看看qiguai.go
}
