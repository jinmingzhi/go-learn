package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	// 自定义输入
	for {
		input := bufio.NewScanner(os.Stdin)
		input.Scan()
		fmt.Println("您输入的内容是：", input.Text())
	}

}
