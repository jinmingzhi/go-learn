package main

import (
	"fmt"
)

func main() {
	y := []int{1, 2, 3, 55, 4, 5}
	for _, v := range y { // _占位符， 去除
		fmt.Println(v)
	}

	z := "avddsad"
	for k, v := range z {
		fmt.Printf("<%d:%c>\n", k, v) // %c 专程ascii 对于的字符
	}
}
