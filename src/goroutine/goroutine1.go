package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan string)

	go getData(ch)
	go sendData(ch)

	time.Sleep(1e9) // 加上定时明显不好，因为你不知道程序什么结束，有可能在此之前，也有可能之后
	// 可以用一个数据标识结束的信号 goroutine2.go
	// fmt.Println(len(ch))
}

func sendData(ch chan string) {
	ch <- "Washington"
	ch <- "Tripoli"
	ch <- "London"
	ch <- "Beijing"
	ch <- "Tokio"
}

func getData(ch chan string) {
	var input string
	// time.Sleep(2e9)
	for {
		input = <-ch
		fmt.Printf("%s ", input)
	}
}
