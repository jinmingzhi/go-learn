package main

import (
	"fmt"
)

func f1(in chan int) {
	fmt.Println(<-in)
}

// chan 不缓存数据，一发对应一收，收或发都会进入堵塞状态，直到有值可收或可发。否则报错
func main() {
	out := make(chan int)
	out <- 2 // 阻塞在了这里，没有地方等待接受数据
	go f1(out)
}

// 上面的处理方法，将协程放在out <- 2 上面就可以了。
/*

func main() {
	out := make(chan int)
	go f1(out) // 启一个协程去就收管道数据
	out <- 2 // 阻塞在了这里，等待数据被接受

}
*/
