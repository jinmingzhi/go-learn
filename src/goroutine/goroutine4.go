package main

import (
	"fmt"
	// "time"
)

// 未完成
func main() {
	ch := make(chan string)
	go sendData(ch)
	getData(ch)
	// time.Sleep(1 * 1e9)
}

func sendData(ch chan string) {
	ch <- "jmz"
	ch <- "jmz1"
	ch <- "jmz2"
	ch <- "jmz3"
	ch <- "jmz4"
	ch <- "jmz5"
	close(ch) // 关闭通道,不关闭的话会报错的.fatal error: all goroutines are asleep - deadlock!
}

func getData(ch chan string) {
	for s := range ch {
		fmt.Println(s)
	}

	// 这种会进入死循环, 有关闭通道就不能使用死循环.因为会阻塞在 获取数据中...
	// for {
	// 	fmt.Println(<-ch)
	// }
}
