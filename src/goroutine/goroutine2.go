package main

import (
	"fmt"
)

func main() {
	ch := make(chan string)

	go sendData(ch)
	getData(ch) // 先执行，并等待接受数据

	// 下面的程序会报错
	// sendData(ch)  // 先发送的数据，但没有地方接收。 chan 一发对应一收， 必须如此，没有办法存数据。发的值没有收，就不可继续发
	// go getData(ch)

}

func sendData(ch chan string) {
	ch <- "Washington"
	ch <- "Tripoli"
	ch <- "London"
	ch <- "Beijing"
	ch <- "Tokio"
	ch <- "end"
}

func getData(ch chan string) {
	var input string
	for {
		input = <-ch
		if input == "end" { // end 表示结束信号
			break
		}
		fmt.Printf("%s ", input)

	}
}

// 以上实现方式的局限性
// 1、只能一个地方发，一个地方收，如果多个地方发送数据呢，结束标识该如何表示
// 2、结束标识一定能够保证是唯一的且不会重复吗？
