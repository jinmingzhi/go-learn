package main

import (
	"fmt"
)

// 空接口类型转换
func echo(i interface{}) {
	fmt.Printf("(%v, %T)\n", i, i)
}

func main() {
	var x interface{}
	x = 1
	echo(x)
	x = map[string]int{"name": 24}
	echo(x)

	y := x.(map[string]int) // 接口的类型转换， go语言一定要记得类型转换，一般不怎么用
	echo(y["name"])

	// ok ture 表示转化成功，转化后的类型;false 表示不成功，z为类型零值。
	z, ok := x.(map[string]int) // map[int]int 这也能转化，但转化的内容却是零值。。。。奇怪
	if ok {
		fmt.Println(z)
	}
	fmt.Println(z)

}
