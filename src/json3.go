package main

import (
	"encoding/json"
	"fmt"
)

// 解码
func main() {
	s := `{"name":"jmz","Age":24,"Hobby":["看书","电影"]}`
	var v interface{}
	err := json.Unmarshal([]byte(s), &v)
	// func Unmarshal(b []byte, v interface{}) err error
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(v) // 需要通过for range 处理数据类型
	//map[name:jmz Age:24 Hobby:[看书 电影]]
}
