package main

import (
	"fmt"
)

func main() {
	// 数组声明的方式有

	var arr1 = [5]int{1, 2, 3, 4, 5}
	arr2 := [3]int{1, 2}
	var arr3 [4]float64
	var arr4 = [5]string{3: "jmz", 4: "jly"}
}
