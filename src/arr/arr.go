package main

import "fmt"

// 数组是值类型传递
// 后面的切片,是引用类型传递
func main() {
	// 静态数组  不能append
	arr := [10]int{1, 2, 3, 4, 5, 6}
	arr[4] = 44 // 数组增改

	// 数组截取 返回数据
	var slice []int // go 的动态数组
	slice = arr[1:3]
	//slice := arr[1:3]   上面两部合成一部
	slice = append(slice, 1, 3, 4, 5) // append 向动态数组添加元素

	fmt.Println(arr)
	fmt.Println(slice)

	// make
	s := make([]int, 3, 5)
	// 3 便是占用了3个空间 len() == 3
	// 5 是cap 容量，超出会增大
	fmt.Printf("%v", s)
	// [0 0 0]
	fmt.Println(cap(s)) // 5
}
