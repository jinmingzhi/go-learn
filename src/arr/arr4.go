package main

import(
	"fmt"
)

// 直接继承切片的变动会影响切片的变化
// 间接切片的增操作会重新产生一个内存空间


func main(){
	arr := [...]int{1,2,3,4,5,6}
	fmt.Println("arr[2:4]=",arr[2:4])
	fmt.Println("arr[2:]=",arr[2:])
	s1 := arr[:4]   // s1 此时只是arr 的视图
	//fmt.Println("arr[:4]=",s1)
	//s2 := arr[:]
	//fmt.Println("arr[:]=",arr[:])

	s1[2] = 100  // s1的变化影响到了arr
	s1 = append(s1,111,222)
	s1[1] = 1010
	fmt.Println(s1)
	fmt.Println(arr)


	s2 := s1[3:5]
	s2 = append(s2,9,8,7)  // 新造了一个内存数据。

	fmt.Println(s2)
	fmt.Println(arr)


	/*

	arr  ------->   [1   2   3   4   5   6]
	s1   ------->   [1   2   3   4]
	     ------->   [1   2   100 4] <------ s1 是arr 的视图所以此时arr --->[1  2   100  4   5  6]
		-------->   [1   2   100 4  111  222]  <--- 此时也是arr 的样子

	s2   ------->                [4 111]
	     ------->                [4 111 9  8  7]   <--- 此时脱离了arr ,新产生了内存数据


	 */
}
