package main

import (
	"fmt"
)

func sum(nums *[5]int) int { // 传指针，使用同一份值
	sum := 0
	for _, num := range nums {
		sum += num
	}

	nums[1] = 11 // 改变数组内下标1 的值，
	return sum
}

func change(nums [5]int) { // 传值，等于重新复制一份数据
	nums[1] = 100
}

func main() {
	arr := [5]int{1, 2, 3, 4} // arr = [1,2,3,4,0]
	fmt.Println(sum(&arr))
	fmt.Println(arr) // [1 11 3 4 0]

	var arr1 [5]int
	change(arr1)
	fmt.Println(arr1) // [0,0,0,0,0]
}
