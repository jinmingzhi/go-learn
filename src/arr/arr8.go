package main

import (
	"fmt"
	"reflect"
)

func main() {
	var arr1 *[5]int
	arr1 = new([5]int)
	arr2 := *arr1 // 值类型拷贝
	arr2[0] = 100
	fmt.Println(*arr1, reflect.TypeOf(arr2)) // [0 0 0 0 0] [5]int
}
