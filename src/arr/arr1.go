package main

import (
	"fmt"
)

// 数组是值类型传递
func sum(nums [5]int) int {
	sum := 0
	for _, num := range nums {
		sum += num
	}
	return sum
}

func main() {
	// go 语言中的 数组和切片是有区别的
	// 数组len 和cap 是固定的。
	arr := [5]int{1, 2, 3, 4, 5}
	fmt.Println(sum(arr))
	fmt.Println("arr=", arr, ",len(arr)=", len(arr), ",cap(arr)=", cap(arr))
	// arr= [1 2 3 4 5] ,len(arr)= 5 ,cap(arr)= 5

	// 数组内的值初始为0
	var arr_init [3]int
	fmt.Println(arr_init) //[0 0 0]

	// arr1 := [3]int{1, 2, 3}
	// fmt.Println(sum(arr1)) // 会报错，函数允许参数是 [5]int 类型
	// cannot use arr1 (type [3]int) as type [5]int in argument to sum

	// arr2 := []int{1, 2, 3, 4}  // 切片也不可以
	// fmt.Println(sum(arr2))

}
