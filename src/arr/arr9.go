package main

import (
	"fmt"
)

const (
	WIDTH  = 5
	HEIGHT = 6
)

type pixel int

var screen [WIDTH][HEIGHT]pixel

func main() {
	for y := 1; y < HEIGHT; y++ {
		for x := 1; x < WIDTH; x++ {
			screen[x][y] = pixel(x * y)
		}
	}
	fmt.Println(screen)
}
