package main

import (
	"fmt"
)

func main() {
	arr := []int{1, 2, 3}
	arr = append(arr, 1)
	fmt.Printf("arr = %v,len(arr)=%d,cap(arr)=%d\n", arr, len(arr), cap(arr))
	// arr = [1 2 3 1],len(arr)=4,cap(arr)=6

	s1 := arr[3:6]
	s1[0] = 100  // 影响了arr的部分
	// s1[4] = 111    // 超出cap 容器范围会报错
	fmt.Printf("s1 = %v,len(s1)=%d,cap(s1)=%d\n", s1, len(s1), cap(s1))
	// s1 = [100],len(s1)=1,cap(s1)=3

	s2 := s1[:2]
	s2[1] = 200
	s2 = append(s2, 333, 444, 555)
	fmt.Println(s1) //[100 200 0]
	fmt.Printf("s2 = %v,len(s2)=%d,cap(s2)=%d\n", s2, len(s2), cap(s2))

	fmt.Printf("arr = %v,len(arr)=%d,cap(arr)=%d\n", arr, len(arr), cap(arr))
	// arr = [1 2 3 100],len(arr)=4,cap(arr)=6
}
