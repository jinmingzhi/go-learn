package main

import (
	"fmt"
)

func main() {

	// 不同的类型有不同的初始值
	var arr1 = [5]int{1, 2, 3}               //[1 2 3 0 0]
	var arr2 = [...]int{1, 2, 3, 4}          // [1 2 3 4]
	var arr3 = []int{1, 2, 3, 4}             // [1 2 3 4]
	var arr4 = []string{3: "jmz", 4: "aaa"}  //[   jmz aaa]
	var arr5 = [7]string{3: "jmz", 4: "aaa"} // [   jmz aaa  ]

	fmt.Println(arr1)
	fmt.Println(arr2)
	fmt.Println(arr3)
	fmt.Println(arr4)
	fmt.Println(arr5)
}
