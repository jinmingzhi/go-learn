package main

import (
	"fmt"
)

// 将数组传递给函数
// 把一个大数组传递给函数会消耗很多内存。有两种方法可以避免这种现象：
// 传递数组的指针
// 使用数组的切片

func main() {
	array := [10]float64{2.4, 1.3, 4.5, 44.11, 1.12, 2.33}
	fmt.Println(Sum(array))
}

func Sum(a [10]float64) (sum float64) {
	for _, v := range a {
		sum += v
	}
	return
}
