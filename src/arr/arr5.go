package main

import(
	"fmt"
)

// 切片的增删改查

func main(){

	/* 三种创建切片的方式 */
	//arr := []int{1,2,3,4,5,6}
	var arr []int
	//arr := make([]int,0)


	/* 增 */
	arr = append(arr,1,2,3,4,5,6)


	/* 改 */
	arr[1] = 100

	/* 删 */
	arr = append(arr[:3],arr[4:]...)   // 后加... 打撒
	arr = append(arr[1:2],arr[len(arr)-1:]...)

	/* 查 */
	s1 := arr[0]
	fmt.Println(s1)

	var copy_arr []int
	copy_arr = append(copy_arr,0)
	copy(copy_arr,arr)  // len(copy_arr) ==1 ,arr 只会把arr的前一个数据copy给copy_arr

	fmt.Println(arr)
	fmt.Println(copy_arr)
	/*
	100
	[100 6]
	[100]
	 */
}
