package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println(time.Now().Unix())                     // 时间戳
	fmt.Println(time.Now().Format("2006-01-02 15:04")) // 当前格式化时间

	// 时间戳转格式化
	fmt.Println(time.Unix(time.Now().Unix(), 0))                        //2018-08-31 17:29:33 +0800 CST
	fmt.Println(time.Unix(1535558005, 0).Format("2006-01-02 15:04:05")) //2018-08-29 23:53:25

	// 格式化转时间戳
	time_unix, err := time.Parse("2006-01-02", "2018-08-29")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(time_unix.Unix()) //1535558005
	}

}
