package main

import (
	"bufio"
	"fmt"
	"os"
)

var inputReader *bufio.Reader

func main() {
	inputReader = bufio.NewReader(os.Stdin)
	fmt.Println("please enter som input:")
	input, err := inputReader.ReadString('\n')
	if err != nil {
		panic(err)
	}
	fmt.Printf("the input was: %s\n", input)
}
