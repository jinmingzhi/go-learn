package main

import (
	"fmt"
)

// 查看各个数据类型的初始值
var (
	i int
	f float64
	s string
	b bool
	r rune
)

func main() {
	fmt.Println(i) // 0
	fmt.Println(f) // 0
	fmt.Println(s) //
	fmt.Println(b) // false
	fmt.Println(r) // 0
}
