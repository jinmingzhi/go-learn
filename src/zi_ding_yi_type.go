package main

import (
	"fmt"
	"reflect"
)

// 自定义数据类型，无法通过interface 转换成其他类型？？？？？？？

type str string   // 创建一个 str 类型
var s str = "jmz" // s 为str 类型，并初始化“jmz”

func main() {
	var i interface{} // 定义一个 接口变量
	i = s

	fmt.Println(i, reflect.TypeOf(i))

	s1, ok := i.(string)
	if ok {
		fmt.Println(s1, reflect.TypeOf(s1))
	} else {
		fmt.Println("没成功")
	}

}
