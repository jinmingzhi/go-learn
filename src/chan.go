package main

import (
	"fmt"
	"strconv"
	"time"
)

// channel 声明 方式
// cn <- c   // 写
// c:= cn    // 读
// 堵塞  除非有goroutine 对其进行操作

func read(cn chan int) {
	value := <-cn                               // 没有数据会堵塞在这里，直到有数据存在
	fmt.Println("value:" + strconv.Itoa(value)) // 输入int 型 转string
}

func write(cn chan int) {
	cn <- 10
}

func main() {
	var cn chan int = make(chan int)
	// cn := make(chan int)
	go read(cn)
	go write(cn)

	time.Sleep(10000000)       // 如果没有这个停止时间，就不会打印出来之前那个value,
	fmt.Println("end of code") // 程序结束，go read(cn) 还没有处理完就结束了。所以没有打印出来。
}
