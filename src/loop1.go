package main

import (
	"fmt"
	"time"
)

func main() {

	var i int = 0

	goto LOOP
	fmt.Println("我会被跳过，不被执行")

LOOP:
	go func(a int) {
		fmt.Println(a)
	}(i)
	i++
	if i < 10 {
		goto LOOP
	}
	time.Sleep(1 * time.Second) // 当我不在10 以内时就会执行

}
