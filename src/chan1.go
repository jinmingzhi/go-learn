package main

import "fmt"

func Add(cn chan int) {
	cn <- 10
}

func main() {
	chs := make([]chan int, 10) // 这种声明 需要len
	for i := 0; i < 10; i++ {
		chs[i] = make(chan int)
		go Add(chs[i]) // 只是发起了有一个指令交由操作系统完成
	}

	for _, ch := range chs {
		fmt.Println(<-ch) // 这样一直等着 ch 有数据（阻塞在这边，不结束线程），
	}

	/**
	这种方式就实现了不用time,Sleep的等待，更有效利用了时间。
	将写的操作给了协程，读的操作给了自己。保证程序的读不到数据不关闭的线程的情况
	*/
}
