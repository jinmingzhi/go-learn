package main

import (
	"fmt"
	"net/http"
)

func hello(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("hello jmz"))
}

func world(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("hello world"))
}

func main() {
	http.HandleFunc("/hello", hello)
	http.HandleFunc("/world", world)

	http.ListenAndServe("127.0.0.1:8188", nil)
}
