package main

import (
	"fmt"
)

/*
iota 只能在常量中使用
1 iota 在const 关键字出现时将被重置为0

2 const 中每新增一行常量生命将使iota 计数一次

3 iota 常用使用法
	1 ) 跳值使用法
	2 ) 插队使用法
	3 ) 表达式隐式使用法
	4 ) 单行使用法
*/

const a = iota
const (
	b  = iota         // 0
	b1                // 1
	b2                // 2
	b3 string = "jmz" //"jmz"	  3
	b4        = 11    // 11	  4
	b5        = iota  // 5
)
const c = iota // iota 在const 关键字出现时将被重置为0

// 跳值使用法
const (
	d  = iota // 0
	d1 = iota // 1
	_         // 2  丢掉
	_         // 3  丢掉
	d2 = iota // 4
)

// 插队使用法
const (
	e  = iota
	e1 = 3.14
	e2 = iota
)

//表达式隐式使用法
// 当常量没有表达式式会自动向上使用非空表达式
const (
	f  = iota * 2 // 0 * 2 = 0
	f1            // 1 * 2 = 2    // 使用上面的表达式， 相当于继承了上面的表达式
	f2            // 2 * 2 = 4
	f3            // 3 * 2 = 6
	f4 = iota * 3 // 4 * 3 = 12   // 新的表达式
	f5            // 5 * 3 = 15   // 使用上面的表达式
	f6 = "aa"     // 又一个新的表达式
	f7            // 继承上面的表达式
)

const (
	g  = "123"
	g1 // 继承上的非空表达式
	g2 // 和上面一样
)

// 单行计数法
// 每一行才会 + 1
// h2 会继承h      h3 会继承 h1 ,  空表达式继承一一继承上一行的非空表达式
const (
	h, h1  = iota, iota + 3     // 0 0+3	//
	h2, h3                      // 1 1+3    //
	h4, h5 = iota * 2, iota + 3 // 2*2 2+3
	h6     = iota               // 3
)

func main() {
	fmt.Println(a)
	fmt.Println(c) //0

	fmt.Println(b)  //0
	fmt.Println(b1) //1
	fmt.Println(b2) //2
	fmt.Println(b3) //"jmz"
	fmt.Println(b4) //11
	fmt.Println(b5) //5

	fmt.Println(d)  //0
	fmt.Println(d1) //1
	fmt.Println(d2) //4

	fmt.Println(f)  //0
	fmt.Println(f1) //2
	fmt.Println(f2) //4
	fmt.Println(f3) //6
	fmt.Println(f4) //12
	fmt.Println(f5) //15
	fmt.Println(f6) //15
	fmt.Println(f7) //15

	fmt.Println(g)  //123
	fmt.Println(g1) //123
	fmt.Println(g2) //123

	fmt.Println(h)  //123
	fmt.Println(h1) //123
	fmt.Println(h2) //123
	fmt.Println(h3) //123
	fmt.Println(h4) //123
	fmt.Println(h5) //123
	fmt.Println(h6) //123

}
