package main

import (
	"fmt"
	"time"
)

//  协程
//  语法 go + 函数名：启动一个协程执行函数体

var x int = 0

func test_routie(x *int) {
	*x += 1
	fmt.Println(*x)
}

func main() {
	for i := 0; i < 100; i++ {
		go test_routie(&x)
	}
	time.Sleep(1000000000) // 5秒 == 5 000 000 000
	fmt.Println("end...")
}
