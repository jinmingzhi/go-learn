package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	inputReader := bufio.NewReader(os.Stdin)
	fmt.Println("please your name:")
	input, err := inputReader.ReadString('\n')
	if err != nil {
		panic(err)
		return
	}

	switch input {
	case "Philip\n":
		fmt.Println("Welcome Philip")
	case "Charis\n":
		fmt.Println("Welcome Charis")
	case "Ivo\n":
		fmt.Println("Welcome Ivo")
	default:
		fmt.Println("default")
	}
}
