package main

import (
	"fmt"
	"test"
)

func main() {
	test.Hello()

	// 下面的静态数组，数量固定没有动
	var arr [10]int
	arr[0] = 1
	arr[2] = 2

	arr1 := [10]int{1, 2, 3, 4} //简短申明定义

	fmt.Println(arr, len(arr))
	fmt.Println(arr1)
}
