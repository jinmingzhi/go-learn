package main

import "fmt"

// 定义函数类型

func main() {
	type typefunc func(x, y int) int //  使用type 关键字 自创出一个typefunc 函数数据类型
	var f typefunc                   // f变量名  typefunc 函数数据类型
	f = func(x, y int) int {         // 变量名赋值匿名函数体
		return x + y

	}
	fmt.Println(f(1, 3))

}
