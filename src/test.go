package main

import (
	"fmt"
	"reflect"
)

func main() {
	// var x []string
	// var y interface{}
	// x := []string(y)
	// fmt.Println(x)
	var x interface{}
	x = []string{"jmz", "jly"}
	if y, ok := x.([]string); ok {
		fmt.Println(y[1], reflect.TypeOf(y[1]))
	}
}
