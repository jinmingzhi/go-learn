package controllers

import (
	"github.com/astaxie/beego"
)

type AddController struct {
	beego.Controller
}

func (c *AddController) Get() {
	c.Data["Title"] = "添加数据"
	c.TplName = "add.tpl"
}
